//
// Created by main on 21.09.2020.
//

#include <Component.h>
#include <Leaf.h>
#include <Composite.h>
#include <iostream>
#include <vector>
#include <algorithm>

int main() {
    std::string input_string;
    std::cout << "Input continued fraction: ";
    std::getline(std::cin, input_string);

    std::string::iterator it = input_string.begin();

    Component *tree = new Composite;
    Component *iter = new Composite;
    Component *iter1;

    std::remove_if(input_string.begin(), input_string.end(), isspace);

    // First iteration
    tree -> Add(new Leaf(*it - '0'));
    tree -> Add(iter);
    it++;

    while(it < (input_string.end() - 1)){
        if(isdigit(*it)) {
            iter1 = new Composite;
            iter -> Add(new Leaf(*it - '0'));
            iter -> Add(iter1);
            iter = iter1;
        }
        it++;
    }

    // End composite have Leaf only
    iter -> Add(new Leaf(*it - '0'));

    std::cout << tree -> Show();

    delete tree;

    return 0;
}