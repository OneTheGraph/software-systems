//
// Created by main on 21.09.2020.
//

#ifndef PRACTICE_1_COMPOSITE_CPP
#define PRACTICE_1_COMPOSITE_CPP

#include <list>
#include <Component.cpp>

class Composite : public Component {
protected:
    std::list<Component *> children_;
	int b_i;
public:
    friend class CompositeBuilder;

    void Add(Component *component) override {
        this->children_.push_back(component);
        component->SetParent(this);
    }

    void Remove(Component *component) override {
        children_.remove(component);
        component->SetParent(nullptr);
    }

    bool IsComposite() const override {
        return true;
    }

    void SetB_i(int b) {
        b_i = b;
    }

    std::string Show() const override {
        std::string result;
        for (const Component *c : children_) {
            if(c == children_.front()) {
                result += c->Show();
            } else {
                if(children_.front() != NULL) {
                    result += "+" + std::to_string(b_i) + "/(" + c->Show() + ")";
                }
            }
            /*} else if (c == children_.back()) {
                result += "+1/(" + c->Show() + ")";
            } else {
                result += "+1/(" + c->Show();
            }*/
        }
        return result;
    }
};

#endif //PRACTICE_1_COMPOSITE_CPP
