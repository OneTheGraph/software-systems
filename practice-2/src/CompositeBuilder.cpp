#include <Composite.cpp>
#include <vector>
#include <Leaf.cpp>

class CompositeBuilder {
	private:
		Composite *tree;
		Composite *iter1;
		Composite *iter;

	public:
		Composite* build(int n, std::vector<int> an, std::vector<int> bn = {1}) {
            tree = new Composite;
            iter = new Composite;
            iter1 = new Composite;

            auto it_an = an.begin();
            std::vector<int>::iterator it_bn = bn.begin();

            // First iteration
            tree -> Add(new Leaf(*it_an));
            tree -> Add(iter);
            tree -> SetB_i(*it_bn);
            it_an++;
            if(bn.size() != 1) {
                it_bn++;
            }

            while(it_an < (an.end() - 1)) {
                iter1 = new Composite;
                iter -> Add(new Leaf(*it_an));
                iter -> Add(iter1);
                iter -> SetB_i(*it_bn);
                iter = iter1;
                it_an++;
                if(bn.size() != 1) {
                    it_bn++;
                }
            }

            // End composite have Leaf only
            it_bn++;
            iter -> SetB_i(*it_bn);
            iter -> Add(new Leaf(*it_an));

            return tree;
		}
};
